import React, { useCallback } from 'react';
import styled from 'styled-components/native';

import useCollection from '../../hooks/useCollection';
import Button from '../../components/General/Button';
import Icon from '../../components/General/Icon';
import iconHeartHollow from '../../assets/images/icons/action-heart--hollow.png'
import iconHeartFilling from '../../assets/images/icons/action-heart--filling.png'

const Wrapper = styled.View`
  width: 40px;
  height: 40px;
`;

const HeartWrapper = styled(Button).attrs({
  activeOpacity: 1,
})`
  width: 40px;
  height: 40px;
`;

const Heart = styled(Icon)`
  width: 30px;
  height: 30px;
  margin: 0;
`;

export interface HeartButtonProps {
  id: string;
  style?: any;
};

export default function HeartButton({ id, style }: HeartButtonProps) {
  const { data, add, remove } = useCollection();
  const collected = (data || []).filter((i: string) => i === id).length > 0;

  const onPress = useCallback(() => {
    const action = collected ? remove : add;

    action(id, {
      type: collected ? undefined : 'success',
      title: collected ? '已從收藏清單移除' : '已加入收藏清單',
      undo: collected ? { category: 'collection', action: 'add', params: [id] } : undefined,
    });
  }, [id, collected, add, remove]);

  return (
    <Wrapper style={style}>
      <HeartWrapper onPress={onPress}>
        <Heart source={collected ? iconHeartFilling : iconHeartHollow} />
      </HeartWrapper>
    </Wrapper>
  );
};
