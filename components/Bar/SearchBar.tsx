import React, { RefObject, useRef, useState, useCallback, useEffect, useMemo } from 'react';
import { TextInput } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import styled from 'styled-components/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import Colors from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import useKeyword from '../../hooks/useKeyword';
import Icon from '../General/Icon';
import { TitleText } from '../StyledText';
import Button from '../General/Button';
import iconSearch from '../../assets/images/icons/action-search.png';
import iconDelete from '../../assets/images/icons/action-x.png';
import iconFilter from '../../assets/images/icons/action-filter.png';

const PngIcon = styled(Icon)`
  width: 20px;
  height: 20px;
`;

const Bar = styled.View`
  z-index: 2;
  box-shadow: 0 2px 2px rgba(102, 102, 102, 0.4);
  elevation: 5;
`;

const Content = styled.View`
  flex-direction: row;
  align-items: center;
  height: 65px;
  padding: 0 8px;
`;

const InputContainer = styled.View`
  flex-grow: 1;
  flex-shrink: 1;
  flex-direction: row;
  align-items: center;
  height: 40px;
`;

const SearchInput = styled.TextInput`
  flex-grow: 1;
  flex-shrink: 1;
  height: 40px;
  padding: 0 36px 0 36px;
  border: 1.5px solid;
  font-size: 17px;
  border-radius: 4px;
`;

const ResetButton = styled.TouchableOpacity`
  position: absolute;
  top: 0;
  right: 0;
  width: 40px;
  height: 40px;
`;

const RightButton = styled(Button)`
  flex-shrink: 0;
  width: 60px;
  height: 40px;
  margin: 0 -8px 0 0;
  padding: 0 12px;
`;

export default function SearchBar({ showFilter }) {
  const navigation = useNavigation();
  const colorScheme = useColorScheme();
  const { background, text, meta: metaColor, shadow } = Colors[colorScheme];
  const { top: safeInset } = useSafeAreaInsets();
  const { data, update } = useKeyword();

  const input = useRef<TextInput>(null);
  const [value, setValue] = useState(data);
  const [focus, setFocus] = useState(false);
  const [textExist, setTextExist] = useState(false);

  useEffect(() => setValue(data), [data]);
  useEffect(() => setTextExist(!!value), [value]);

  const onDelete = useCallback(() => setValue(''), []);
  const onReset = useCallback(() => setValue(data), [data]);
  const onFocus = useCallback(() => setFocus(true), []);
  const onBlur = useCallback(() => setFocus(false), []);
  const onSubmit = useCallback(() => update(value), [value, update]);
  const onCancel = useCallback(() => {
    onReset();
    input.current?.blur();
  }, [input, onReset]);
  const onFilter = useCallback(() => {
    navigation.navigate('FilterMain');
  }, [navigation]);

  const focusStyle = focus ? {
    borderColor: '#ffcf3d',
    shadowColor: 'rgb(236, 146, 53)',
    shadowOpacity: 0.2,
    shadowOffset: { width: 4, height: 4 },
  } : {};

  const rightAction = useMemo(() => {
    if(!showFilter) return null;
    const onPress = focus ? onCancel : onFilter;

    return (
      <RightButton onPress={onPress}>
        {focus ?
          <TitleText style={{ color: metaColor }}>取消</TitleText> :
          <Icon style={{ width: 32, height: 32 }} source={iconFilter} />}
      </RightButton>
    );
  }, [focus, onCancel, onFilter, showFilter]);

  return (
    <Bar style={{
      paddingTop: safeInset,
      backgroundColor: background,
      shadowColor: shadow,
    }}>
      <Content>
        <InputContainer>
          <SearchInput
            ref={input as RefObject<TextInput>}
            style={{
              color: text,
              borderColor: metaColor,
              backgroundColor: background,
              ...focusStyle,
            }}
            placeholderTextColor={metaColor}
            value={value}
            onFocus={onFocus}
            onBlur={onBlur}
            onChangeText={setValue}
            onSubmitEditing={onSubmit}
            returnKeyType="search"
            placeholder="搜尋貓食品牌或名稱"
          />
          <PngIcon
            style={{ position: 'absolute', top: 10, left: 10, tintColor: metaColor }}
            source={iconSearch}
          />
          {textExist && focus && (
            <ResetButton onPress={onDelete}>
              <PngIcon source={iconDelete} />
            </ResetButton>
          )}
        </InputContainer>

        {rightAction}
      </Content>
    </Bar>
  );
}
