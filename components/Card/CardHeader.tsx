import React, { useMemo } from 'react';
import { GestureResponderEvent } from 'react-native';
import styled from 'styled-components/native';

import Colors from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import { TitleText, NoteText } from '../StyledText';

const Line = styled.View`
  flex-grow: 1;
  flex-direction: row;
`;

const Title = styled(TitleText)`
  line-height: 26px;
`;

const Meta = styled(NoteText)`
  line-height: 24px;
`;

const Main = styled.View`
  flex-grow: 1;
`;

const Action = styled.View`
  flex-shrink: 0;
  flex-direction: row;
  align-items: center;
  margin: 0 -6px 5px 12px;
`;

export interface CardHeaderProps {
  title: string;
  subtitles: {
    text: string;
    color?: string;
  }[];
  action?: React.ReactNode;
  onPress?: (event: GestureResponderEvent) => void | undefined;
}

export default function CardHeader({ title, subtitles, action, onPress }: CardHeaderProps) {
  const colorScheme = useColorScheme();

  const meta = useMemo(() => subtitles.map(({ text, color }, index) => {
    const fontColor = color || Colors[colorScheme].meta;
    return <Meta key={index} style={{ marginRight: 16, color: fontColor }}>{text}</Meta>;
  }), [subtitles, colorScheme]);

  return (
    <Line>
      <Main>
        <Title>{title}</Title>
        <Line>{meta}</Line>
      </Main>
      {action && <Action>{action}</Action>}
    </Line>
  );
}
