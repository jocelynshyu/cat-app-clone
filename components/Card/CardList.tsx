import React from 'react';
import { ScrollViewProps } from 'react-native';
import styled from 'styled-components/native';

export default styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false,
  scrollEventThrottle: 300,
})`
  padding: 12px 0 0;
`;

export const Space = styled.View`
  height: 62px;
`;
