import styled, { css } from 'styled-components/native';

const cardCss = css`
  flex-direction: row;
  margin: 12px 8px 0;
  padding: 24px 0 24px 24px;
  box-shadow: 0 1px 4px rgba(102, 102, 102, 0.4);
  border-radius: 10px;
  elevation: 2;
`;

export const Card = styled.View`
  ${cardCss}
`;



export const ToggleCard = styled.TouchableOpacity.attrs({
  activeOpacity: 0.8,
})`
  ${cardCss}
`;

export default styled.TouchableOpacity.attrs({
  activeOpacity: 0.5,
})`
  ${cardCss}
`;
