import React, { useState, useCallback, useMemo } from 'react';
import styled from 'styled-components/native';

import Colors from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import { IngredientType } from '../../model/product.interface';
import Icon from '../General/Icon';
import CardWrapper from './CardWrapper';
import CardHeader from './CardHeader';
import IngredientLine from '../Data/IngredientLine';
import iconArrowUp from '../../assets/images/icons/arrow-up.png';
import iconArrowDown from '../../assets/images/icons/arrow-down.png';

const Main = styled.View`
  flex-grow: 1;
`;

const Go = styled.View`
  width: 30px;
  height: 25px;
  margin: 0 12px 0 auto;
  align-self: flex-start;
`;

const ingredientData = [
  { type: 'protein', value: 30, title: '蛋白質' },
  { type: 'fat', value: 20, title: '脂肪' },
  { type: 'carb', value: 40, title: '碳水化合物' },
];

export default function IntroductionCard() {
  const colorScheme = useColorScheme();
  const { text, cardBackground } = Colors[colorScheme];

  const [show, setShow] = useState(false);

  const onPress = useCallback(() => setShow(!show), [show]);

  const arrow = useMemo(() => {
    const source = show ? iconArrowUp : iconArrowDown;
    return <Icon style={{ width: 24, height: 24, tintColor: text }} source={source} />;
  }, [show]);

  const ingredient = ingredientData.map(({ type, value, title }) => ({
    type, value, title: show ? title : undefined,
  } as {
    type: IngredientType, value: number, title: string,
  }));

  return (
    <CardWrapper
      style={{ backgroundColor: cardBackground }}
      onPress={onPress}
    >
      <Main>
        <CardHeader
          title="長條圖代表什麼？"
          subtitles={show ? [{ text: '各貓食的代謝能來源比例。' }] : []}
        />
        <IngredientLine ingredient={ingredient} />
      </Main>
      <Go>{arrow}</Go>
    </CardWrapper>
  );
}
