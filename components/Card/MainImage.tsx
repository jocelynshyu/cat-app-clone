import React, { useCallback, useState } from 'react';
import { Animated, Modal, Platform } from 'react-native';
import styled from 'styled-components/native';
import { useNavigation } from '@react-navigation/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { PanGestureHandler } from 'react-native-gesture-handler';

import Layout from '../../constants/Layout';
import Colors, { fixedColors } from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import { LargeTitleText } from '../StyledText';
import Icon from '../General/Icon';
import Button from '../General/Button';
import HeartButton from '../Action/HeartButton';
import iconArrowLeft from '../../assets/images/icons/arrow-left.png';
import iconClose from '../../assets/images/icons/action-x.png';

const Shadow = styled.View`
  box-shadow: 0 2px 4px rgba(102, 102, 102, 0.4);
  elevation: 2;
`;

const Wrapper = styled(Animated.View)`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  flex-direction: column;
  height: ${Layout.headerHeight}px;
`;

const ImageBox = styled(Shadow)`
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
`;

const Image = styled(Animated.Image)`
  width: 100%;
  height: 100%;
  border-bottom-right-radius: 50px;
  resize-mode: cover;
`;
const ModalImage = styled(Image)`
  position: absolute;
  top: 0;
  left: 0;
  border-bottom-right-radius: 0;
`;

const DragArea = styled(Animated.View)`
  z-index: 2;
  elevation: 2;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  height: 24px;
  margin: auto 0 0;
`;

const DragBar = styled.View`
  width: 80px;
  height: 4px;
  border-radius: 24px;
  background: ${fixedColors.black[0]};
  box-shadow: 0 -1px 0 ${fixedColors.black[2]};
`;

const Line = styled(Animated.View)`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  z-index: 2;
  elevation: 2;
`;

const LineContent = styled.View`
  width: 100%;
  flex-direction: row;
  flex-wrap: wrap;
  align-items: center;
  padding: 0 16px 0 0;
`;

const LineBackground = styled(Shadow)`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
`;

const BackButton = styled(Button)`
  width: 50px;
  height: 50px;
  padding: 0 5px 0 0;
`;

const BackIcon = styled(Icon).attrs({ source: iconArrowLeft })`
  position: absolute;
  width: 30px;
  height: 30px;
  box-shadow: 1px 2px 3px rgba(255, 255, 255, 0.6);
`;
const BackShadowIcon = styled(BackIcon).attrs({ blurRadius: 1.5 })`
  width: 40px;
  height: 40px;
  transform: translateX(0.5px);
`;

const TitleBox = styled(Animated.View)`
  flex-shrink: 1;
  flex-grow: 1;
`;

const Title = styled(LargeTitleText).attrs({ numberOfLines: 1 })``;

const CloseButton = styled(Button)`
  position: absolute;
  top: 0;
  right: 0;
  width: 65px;
  height: 65px;
`;

export interface ProductImageProps {
  scrollY: any;
  id: string;
  title: string;
  mainImage: string;
};

export default function MainImage({ scrollY, id, title, mainImage }: ProductImageProps) {
  const navigation = useNavigation();

  const colorScheme = useColorScheme();
  const { background, text, cardBackground, shadow } = Colors[colorScheme];

  const { top: safeInset, bottom: bottomInset } = useSafeAreaInsets();
  const [show, setShow] = useState(false);

  const onBackPress = useCallback(() => navigation.goBack(), [navigation]);

  const onClose = useCallback(() => setShow(!show), [show]);

  const onGestureEvent = useCallback(({ nativeEvent }) => {
    if (!scrollY) return;
    const shouldToggle = show && nativeEvent.velocityY < 0 || !show && nativeEvent.velocityY > 0;
    if (shouldToggle) setShow(!show);
  }, [scrollY, show]);

  const HEADER_MAX_HEIGHT = Layout.headerHeight + safeInset;
  const HEADER_MIN_HEIGHT = 50 + safeInset;
  const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

  const headerTranslateY = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, -HEADER_SCROLL_DISTANCE],
    extrapolate: 'clamp',
  });

  const imageOpacity = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
    outputRange: [1, 1, 0],
    extrapolate: 'clamp',
  });

  const lineTranslateY = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE],
    outputRange: [0, HEADER_MAX_HEIGHT - safeInset - 50],
    extrapolate: 'clamp',
  });

  const lineOpacity = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE, HEADER_SCROLL_DISTANCE + 10],
    outputRange: [0, 0, 1],
    extrapolate: 'clamp',
  });

  const titleOpacity = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE + 10, HEADER_SCROLL_DISTANCE + 20],
    outputRange: [0, 0, 1],
    extrapolate: 'clamp',
  });

  const heartScale = scrollY.interpolate({
    inputRange: [0, HEADER_SCROLL_DISTANCE + 10, HEADER_SCROLL_DISTANCE + 20, HEADER_SCROLL_DISTANCE + 30],
    outputRange: [0, 0, 1.2, 1],
    extrapolate: 'clamp',
  });

  const dargTranslateY = scrollY.interpolate({
    inputRange: [0, 1],
    outputRange: [0, -HEADER_MAX_HEIGHT * 2],
    extrapolate: 'clamp',
  });

  return (
    <Wrapper
      style={{
        height: HEADER_MAX_HEIGHT,
        backgroundColor: cardBackground,
        transform: [{ translateY: headerTranslateY }],
      }}
    >
      <ImageBox>
        <Image style={{ opacity: imageOpacity }} source={{ uri: mainImage }} />
      </ImageBox>

      <Line style={{ transform: [{ translateY: lineTranslateY }] }}>
        <Animated.View style={{ opacity: lineOpacity }}>
          <LineBackground style={{ height: safeInset + 50, backgroundColor: background, shadowColor: shadow }} />
        </Animated.View>

        <LineContent style={{ paddingTop: safeInset }}>
          <BackButton onPress={onBackPress}>
            {Platform.OS !== 'ios' && <BackShadowIcon style={{ tintColor: background }} />}
            <BackIcon style={{ tintColor: text }} />
          </BackButton>
          <TitleBox style={{ opacity: titleOpacity }}>
            <Title>{title}</Title>
          </TitleBox>

          <Animated.View style={{ transform: [{ scale: heartScale }] }}>
            <HeartButton id={id} />
          </Animated.View>
        </LineContent>
      </Line>

      <PanGestureHandler onGestureEvent={onGestureEvent}>
        <DragArea style={{ transform: [{ translateY: dargTranslateY }] }}>
          <DragBar />
        </DragArea>
      </PanGestureHandler>

      <Modal
        statusBarTranslucent
        animationType="fade"
        visible={show}
      >
        <ModalImage source={{ uri: mainImage }} blurRadius={10} />
        <ModalImage style={{ resizeMode: 'contain' }} source={{ uri: mainImage }} />

        {
          Platform.OS === 'ios' ? (
            <PanGestureHandler onGestureEvent={onGestureEvent}>
              <DragArea style={{ height: 24 + bottomInset, paddingBottom: bottomInset }}>
                <DragBar />
              </DragArea>
            </PanGestureHandler>
          ) : (
            <CloseButton style={{ top: safeInset }} onPress={onClose}>
              <Icon style={{ width: 30, height: 30, tintColor: text }} source={iconClose}/>
            </CloseButton>
          )
        }
      </Modal>
    </Wrapper>
  );
}
