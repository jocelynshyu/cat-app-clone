import React, { useCallback, useMemo } from 'react';
import styled from 'styled-components/native';
import { useNavigation } from '@react-navigation/native';

import Colors from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import Icon from '../General/Icon';
import HeartButton from '../Action/HeartButton';
import CardWrapper from './CardWrapper';
import CardHeader from './CardHeader';
import NumericalLine from '../Data/NumericalLine';
import IngredientLine from '../Data/IngredientLine';
import { Product, IngredientType } from '../../model/product.interface';
import iconArrowRight from '../../assets/images/icons/arrow-right.png';

const PngIcon = styled(Icon)`
  width: 24px;
  height: 24px;
`;

const Main = styled.View`
  z-index: 2;
  flex-grow: 1;
`;

const Go = styled.View`
  z-index: 1;
  width: 48px;
  margin-left: auto;
`;

export default function ProductCard(props: Product) {
  const colorScheme = useColorScheme();
  const { text, cardBackground, link, shadow } = Colors[colorScheme];

  const navigation = useNavigation();
  const {
    _id, brand_CH: brandName, productName_CH: productName, flavor_CH: flavorName,
    proteinMetabolizedRatio: protein, fatMetabolizedRatio: fat, carbsMetabolizedRatio: carb,
  } = props;

  const onPress = useCallback(() => {
    navigation.navigate('Product', props);
  }, [props]);

  const ingredientData = Object.entries({ protein, fat, carb }).map(([type, v]) => {
    const value = Math.round(v * 10) / 10;
    return { type, value, title: `${value}%` } as {type: IngredientType, value: number, title: string};
  });

  return (
    <CardWrapper
      style={{ backgroundColor: cardBackground, shadowColor: shadow }}
      onPress={onPress}
    >
      <Main>
        <CardHeader
          title={flavorName}
          subtitles={[{ text: brandName, color: link }, { text: productName }]}
          action={<HeartButton id={_id} />}
        />
        <NumericalLine {...props} />
        <IngredientLine ingredient={ingredientData} />
      </Main>
      <Go>
        <PngIcon source={iconArrowRight} style={{ tintColor: text }} />
      </Go>
    </CardWrapper>
  );
}
