import React, { useMemo } from 'react';
import { View } from 'react-native';
import styled from 'styled-components/native';

import { ContentText, TitleText } from '../../StyledText';
import Colors from '../../../constants/Colors';
import useColorScheme from '../../../hooks/useColorScheme';
import PercentLine from '../PercentLine';

const Line = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
  height: 32px;
`;

const LabelText = styled(ContentText)`
  margin: 0 auto 0 0;
`;

const ValueText = styled(TitleText)`
  width: 60px;
  text-align: right;
`;

const DividerLine = styled.View`
  height: 1px;
  margin: 12px 0;
`;

type BasisType = 'crudeProtein' | 'crudeFat' | 'crudeCarbs' | 'crudeAsh' | 'crudeFiber';

const items: Array<{ type: BasisType, title: string }> = [
  { type: 'crudeProtein', title: '粗蛋白' },
  { type: 'crudeFat', title: '粗脂肪' },
  { type: 'crudeCarbs', title: '粗碳水化合物' },
  { type: 'crudeAsh', title: '粗灰份' },
  { type: 'crudeFiber', title: '粗纖維' },
];

export default function ProductDataDryMatterBasis(props) {
  const colorScheme = useColorScheme();
  const { text, divider } = Colors[colorScheme];
  const Label = styled(LabelText)`color: ${text};`;
  const Value = styled(ValueText)`color: ${text};`;
  const Divider = styled(DividerLine)`background-color: ${divider};`;

  const list = useMemo(() => items.map(({ type, title }, index) => {
    const value = (Math.round(props[type] * 10) / 10);

    return (
      <React.Fragment key={type}>
        {!!index && <Divider />}
        <Line>
          <Label>{title}</Label>
          <PercentLine percent={value} />
          <Value>{`${value.toFixed(1)}%`}</Value>
        </Line>
      </React.Fragment>
    );
  }), [Label, Value, Divider]);

  return (
    <View>{list}</View>
  );
}
