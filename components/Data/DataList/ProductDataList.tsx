import React, { useState, useCallback, useMemo } from 'react';
import styled from 'styled-components/native';

import Toggle from '../../../components/General/Toggle';
import ProductDataOverview from './ProductDataOverview';
import ProductDataDryMatterBasis from './ProductDataDryMatterBasis';
import ProductDataOthers from './ProductDataOthers';
import ProductDataOfficial from './ProductDataOfficial';

const Wrapper = styled.View`
  padding: 0 0 30px;
`;

export default function ProductDataList(props) {
  const [active, setActive] = useState(undefined);

  const onPress = useCallback((type) => {
    const nextActive = type === active ? undefined : type;
    setActive(nextActive);
  }, [active]);

  const list = [
    { type: 'overview', title: '概覽', children: <ProductDataOverview {...props} /> },
    { type: 'dryMatterBasis', title: '乾物比', children: <ProductDataDryMatterBasis {...props} /> },
    { type: 'others', title: '礦物質與其他成分', children: <ProductDataOthers {...props} /> },
    { type: 'official', title: '官方成分', children: <ProductDataOfficial {...props} /> },
  ];

  const node = useMemo(() => list.map(({ type, children, ...others }) =>
    <Toggle
      key={type}
      active={type === active}
      onPress={onPress}
      type={type}
      {...others}
    >
      {children}
    </Toggle>
  ), [list, active, onPress]);

  return <Wrapper>{node}</Wrapper>;
}
