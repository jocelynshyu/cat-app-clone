import React from 'react';
import styled from 'styled-components/native';

import { ContentText } from '../../StyledText'

const Wrapper = styled.View`
  margin: -6px 0 0;
`;

export default function ProductDataOfficial({ ingredients_CH }) {
  return (
    <Wrapper>
      <ContentText>{ingredients_CH}</ContentText>
    </Wrapper>
  );
}
