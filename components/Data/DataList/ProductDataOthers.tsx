import React from 'react';
import styled from 'styled-components/native';

import { Product } from '../../../model/product.interface';
import { NoteText, ContentText, TitleText } from '../../StyledText';
import Colors from '../../../constants/Colors';
import useColorScheme from '../../../hooks/useColorScheme';
// import Tag from '../../General/Tag';

const MetaText = styled(NoteText)`
  margin: 4px 0 0;
  text-align: right;
`;

const Line = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
  height: 32px;
`;

const LabelText = styled(ContentText)`
  margin: 0 auto 0 0;
`;

// const DataTag = styled(Tag)`
//   margin: 0 16px 0 0;
// `;

const DividerLine = styled.View`
  height: 1px;
  margin: 12px 0;
`;

// <DataTag primary theme="green" text="適中" />
// <Meta style={{ marginRight: -6 }}>{`（ ${phosphorus} 克 / 100 大卡 ）`}</Meta>

const list = [
  { type: 'ca_PRatio', title: '鈣磷比', suffix: ': 1' },  // 「適中」標籤
  { type: 'calcium', title: '鈣', suffix: '克' },
  { type: 'phosphorus', title: '磷', suffix: '克' },      // 「適中」標籤 / meta
  { type: 'sodium', title: '鈉', suffix: '克' },
  { type: 'magnesium', title: '鎂', suffix: '克' },
];

const getText = (value, suffix) => {
  if (!value && value !== 0) return '-';
  if (suffix !== '克') return `${value} ${suffix}`;

  const v = (Math.round(value * 100) / 100).toFixed(2);
  return `${v} ${suffix}`;
};

export default function ProductDataOthers(props: Product) {
  const colorScheme = useColorScheme();
  const { text, meta, divider } = Colors[colorScheme];

  const Meta = styled(MetaText)`color: ${meta};`;
  const Label = styled(LabelText)`color: ${text};`;
  const Value = styled(TitleText)`color: ${text};`;
  const Divider = styled(DividerLine)`background-color: ${divider};`;

  return (
    <>
      <Meta style={{ marginTop: 0, marginBottom: 5 }}>/ 100 克</Meta>
      {
        list.map(({ type, title, suffix }, index) => {
          const text = getText(props[type], suffix);

          return (
            <React.Fragment key={type}>
              {!!index && <Divider />}
              <Line>
                <Label>{title}</Label>
                <Value>{text}</Value>
              </Line>
            </React.Fragment>
          );
        })
      }
      {/* <Line>
        <Label>牛磺酸</Label>
        <Value>有</Value>
      </Line>
      <Line>
        <Label>Omega 3</Label>
        <Value>有</Value>
      </Line>
      <Line>
        <Label>維他命 K3</Label>
        <Value>沒有</Value>
      </Line> */}
    </>
  );
}
