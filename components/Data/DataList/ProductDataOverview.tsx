import React from 'react';
import styled from 'styled-components/native';

import { NoteText } from '../../StyledText';
import Colors from '../../../constants/Colors';
import useColorScheme from '../../../hooks/useColorScheme';
import Tag from '../../General/Tag';

const Box = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
`;

const DataTag = styled(Tag).attrs({
  large: true,
})`
  margin: 8px 8px 0 0;
`;

const DividerLine = styled.View`
  height: 1px;
  margin: 12px 0;
`;

const list = [
  {
    type: 'cereals',
    title: '是否含有穀類？',
    tags: (arr) => {
      if (!arr || !arr.length) return [{ text: '無穀', theme: 'green', primary: true }];
      return [
        { text: '含有穀類', primary: true },
        ...arr.map(text => ({ text })),
      ].map(t => ({ ...t, theme: 'yellow' }));
    },
  }, {
    type: 'thickener',
    title: '是否含有膠類或增稠劑？',
    tags: (arr) => {
      if (!arr || !arr.length) return [{ text: '無膠類或增稠劑', theme: 'green', primary: true }];
      return [
        { text: '含有膠類或增稠劑', primary: true },
        ...arr.map(text => ({ text })),
      ].map(t => ({ ...t, theme: 'yellow' }));
    },
  },
];

export default function ProductDataOverview(props) {
  const colorScheme = useColorScheme();
  const { text, divider } = Colors[colorScheme];

  const Title = styled(NoteText)`color: ${text};`;
  const Divider = styled(DividerLine)`background-color: ${divider};`;

  return (
    <>
      {
        list.map(({ type, title, tags }, index) => (
          <React.Fragment key={type}>
            <Title>{title}</Title>
            <Box>
              {tags(props[type]).map(t => <DataTag key={t.text} {...t} />)}
            </Box>
            <Divider />
          </React.Fragment>
        ))
      }
      {/* <Divider />
      <Title>是否含有人工糖分？</Title>
      <Box>
        <DataTag primary theme="yellow" text="含有人工糖分" />
        <DataTag theme="yellow" text="玉米糖漿" />
      </Box>
      <Divider />
      <Title>蛋白質來源</Title>
      <Box>
        <DataTag primary theme="green" text="肉是主要成分" />
        <DataTag primary theme="yellow" text="肉不是主要成分" />
        <DataTag primary theme="green" text="蛋白質來源標示清楚" />
        <DataTag primary theme="yellow" text="蛋白質來源標示不清楚" />
        <DataTag primary theme="green" text="不含動物副產品" />
        <DataTag primary theme="yellow" text="含動物副產品" />
        <DataTag theme="green" text="蟑螂腳" />
        <DataTag theme="red" text="人肉" />
        <DataTag theme="red" text="魚肉" />
      </Box> */}
    </>
  );
}
