import React from 'react';
import styled from 'styled-components/native';

import Colors, { fixedColors }from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import { Ingredient } from '../../model/product.interface';
import { LargeTitleText, ContentText, NoteText } from '../StyledText';
import Icon from '../General/Icon';
import { Card } from '../Card/CardWrapper';
import iconProtein from '../../assets/images/icons/food-meat.png';
import iconOil from '../../assets/images/icons/food-oil.png';
import iconCarb from '../../assets/images/icons/food-carbs.png';

const Wrapper = styled(Card)`
  flex-direction: column;
  margin: 30px 0;
  padding: 16px;
`;

const Title = styled(NoteText)`
  line-height: 20px;
`;

const Main = styled.View`
  flex-direction: row;
  margin: 8px 0 0;
`;

const SubTitle = styled.View`
  flex-direction: row;
  align-items: center;
`;

const FoodIcon = styled(Icon)`
  width: 28px;
  height: 28px;
  margin: 0 0 0 -6px;
`;

const Text = styled(ContentText)`
  margin: 0 0 0 4px;
`;

const NumberText = styled(LargeTitleText)`
  margin: 10px 0 0;
`;

const Block = styled.View`
  flex-shrink: 0;
  flex-grow: 1;
  width: 0;
  padding: 12px 6px 8px;
`;

const Line = styled.View`
  flex-shrink: 0;
  align-self: stretch;
  width: 1px;
  margin: 0 10px;
  background-color: ${fixedColors.black[4]};
  opacity: 0.5;
`;

function number(value) {
  if (!value) return '-';
  return Math.round(value * 10) / 10;
}

export interface IngredientBlockProps {
  ingredient: Ingredient;
}

export default function IngredientBlock({ ingredient }: IngredientBlockProps) {
  const colorScheme = useColorScheme();
  const { text, meta, cardBackground, shadow } = Colors[colorScheme];

  return (
    <Wrapper style={{ backgroundColor: cardBackground, shadowColor: shadow }}>
      <Title style={{ color: meta }}>代謝能來源比例</Title>

      <Main>
        <Block style={{ flexGrow: 3 }}>
          <SubTitle>
            <FoodIcon source={iconProtein} />
            <Text style={{ color: text }}>蛋白質</Text>
          </SubTitle>
          <NumberText>{number(ingredient.protein) || '- '}%</NumberText>
        </Block>

        <Line />

        <Block style={{ flexGrow: 3 }}>
          <SubTitle>
            <FoodIcon source={iconOil} />
            <Text style={{ color: text }}>脂肪</Text>
          </SubTitle>
          <NumberText>{number(ingredient.fat) || '- '}%</NumberText>
        </Block>

        <Line />

        <Block style={{ flexGrow: 4 }}>
          <SubTitle>
            <FoodIcon source={iconCarb} />
            <Text style={{ color: text }}>碳水化合物</Text>
          </SubTitle>
          <NumberText>{number(ingredient.carb) || '- '}%</NumberText>
        </Block>
      </Main>
    </Wrapper>
  );
}
