import React, { useMemo } from 'react';
import styled from 'styled-components/native';

import Colors, { fixedColors } from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import { IngredientType } from '../../model/product.interface';
import { TitleText } from '../StyledText';

const TextBox = styled.View`
  flex-direction: row;
`;

const Text = styled(TitleText).attrs({
  numberOfLines: 1,
})`
  min-width: 60px;
  margin: 0 0 4px;
  line-height: 26px;
`;

const SmallText = styled(Text)`
  text-align: right;
`;

const Box = styled.View`
  flex-direction: row;
  margin: 12px -4px 0 0;
`;

const Block = styled.View`
  width: 0;
  margin: 0 4px 0 0;
`;

const SmallBlock = styled(Block)`
  width: 16px;
`;

const Line = styled.View`
  height: 4px;
  border-radius: 12px;
`;


export interface IngredientLineProps {
  ingredient: {
    type: IngredientType;
    value: number;
    title?: string;
  }[];
}

const colors = {
  protein: fixedColors.green[5],
  fat: fixedColors.orange[5],
  carb: fixedColors.red[5],
};

export default function IngredientLine({ ingredient }: IngredientLineProps) {
  const colorScheme = useColorScheme();

  const segments = ingredient.filter(({ value }) => value || value === 0);

  if (!segments || !segments.length) {
    return (
      <Box>
        <Block style={{ flexGrow: 1 }}>
          <Text></Text>
          <Line style={{ backgroundColor: Colors[colorScheme].divider }} />
        </Block>
      </Box>
    );
  }

  return (
    <Box>
      {
        segments.map(({ type, value, title }) => {
          const small = value <= 5;
          const BlockEle = small ? SmallBlock : Block;
          const textRight = value <= 15;
          const TextEle = textRight ? SmallText : Text;

          return (
            <BlockEle key={type} style={{ flexGrow: small ? 0 : value }}>
              {title && (
                <TextBox style={{ justifyContent: textRight ? 'flex-end' : undefined }}>
                  <TextEle style={{ color: Colors[colorScheme].text }}>
                    {title}
                  </TextEle>
                </TextBox>
              )}
              <Line style={{ backgroundColor: colors[type] }} />
            </BlockEle>
          );
        })
      }
    </Box>
  );
}
