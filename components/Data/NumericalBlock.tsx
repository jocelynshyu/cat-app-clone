import React from 'react';
import styled from 'styled-components/native';

import Colors, { fixedColors } from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import { LargeTitleText, NoteText } from '../StyledText';
import Icon from '../General/Icon';
import { Card } from '../Card/CardWrapper';
import { Numerical } from '../../model/product.interface';
import iconFire from '../../assets/images/icons/food-fire.png';
import iconWater from '../../assets/images/icons/food-water.png';

const PngIcon = styled(Icon)`
  width: 32px;
  height: 32px;
  margin: 5px 10px 0 5px;
`;

const Wrapper = styled.View`
  flex-direction: column;
  margin: 20px 0 0;
`;

const Block = styled(Card)`
  align-items: center;
  height: 70px;
  margin: 0;
  padding: 12px;
  border-radius: 0;
`;

const BlockTop = styled(Block)`
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
`;

const BlockBottom = styled(Block)`
  margin: 3px 0 0;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
`;

const Line = styled.View`
  width: 10px;
  height: 50px;
  margin: 0 8px;
  background: ${fixedColors.orange[4]};
  border-radius: 16px;
  opacity: 0.3;
`;

const Meta = styled.View`
  margin: 2px 0 0;
`;

const LastText = styled(NoteText)`
  margin: 20px 0 0 auto;
`;

export default function NumericalBlock({ metabolismEnergy, moisture }: Numerical) {
  const colorScheme = useColorScheme();
  const { meta, subCardBackground, shadow } = Colors[colorScheme];

  const blockStyle = { backgroundColor: subCardBackground, shadowColor: shadow };

  return (
    <Wrapper>
      <BlockTop style={blockStyle}>
        <Line />
        <PngIcon source={iconFire} />
        <Meta>
          <NoteText style={{ color: meta }}>代謝能</NoteText>
          <LargeTitleText>{(Math.round(metabolismEnergy * 10) / 10).toFixed(1)}</LargeTitleText>
        </Meta>
        <LastText style={{ color: meta }}>大卡 / 100克</LastText>
      </BlockTop>

      <BlockBottom style={blockStyle}>
        <Line />
        <PngIcon source={iconWater} />
        <Meta>
          <NoteText style={{ color: meta }}>水量</NoteText>
          <LargeTitleText>{(Math.round(moisture * 10) / 10).toFixed(1)}</LargeTitleText>
        </Meta>
        <LastText style={{ color: meta }}>克 / 100克</LastText>
      </BlockBottom>
    </Wrapper>
  );
}
