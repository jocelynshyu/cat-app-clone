import React from 'react';
import styled from 'styled-components/native';

import Colors from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import { TitleText } from '../StyledText';
import Icon from '../General/Icon';
import { Numerical } from '../../model/product.interface';
import iconFire from '../../assets/images/icons/food-fire.png';
import iconWater from '../../assets/images/icons/food-water.png';

const PngIcon = styled(Icon)`
  width: 24px;
  height: 24px;
  margin: 0 6px 0 -2px;
`;

const Line = styled.View`
  flex-direction: row;
  align-items: center;
  margin: 12px 0 0;
`;

const Block = styled(Line)`
  margin: 0 12px 0 0;
`;

export default function NumericalLine({ metabolismEnergy, moisture }: Numerical) {
  const colorScheme = useColorScheme();

  const { meta } = Colors[colorScheme];

  return (
    <Line>
      <Block>
        <PngIcon source={iconFire} />
        <TitleText>{(Math.round(metabolismEnergy * 10) / 10).toFixed(1)}</TitleText>
        <TitleText style={{ marginLeft: 2, color: meta }}> 大卡</TitleText>
      </Block>
      <Block>
        <PngIcon source={iconWater} />
        <TitleText>{(Math.round(moisture * 10) / 10).toFixed(1)}</TitleText>
        <TitleText style={{ marginLeft: 2, color: meta }}> 克</TitleText>
      </Block>
      <TitleText style={{ marginLeft: 'auto', color: meta }}>/ 100 克</TitleText>
    </Line>
  );
}
