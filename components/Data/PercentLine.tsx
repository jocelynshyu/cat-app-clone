import React, { useMemo } from 'react';
import styled from 'styled-components/native';

import Colors from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';

const WrapperBlock = styled.View`
  width: 120px;
  height: 8px;
  border: 1px solid;
  border-radius: 12px;
`;

const LineBlock = styled.View`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  border-radius: 12px;
`;

export interface PercentLineProps {
  percent: number;
}

export default function PercentLine({ percent }: PercentLineProps) {
  const colorScheme = useColorScheme();
  const { meta } = Colors[colorScheme];

  const length = Math.max(percent, 5);

  const Wrapper = styled(WrapperBlock)`borderColor: ${meta}`;
  const Line = styled(LineBlock)`width: ${length}%; backgroundColor: ${meta}`;

  return (
    <Wrapper>
      <Line />
    </Wrapper>
  );
}
