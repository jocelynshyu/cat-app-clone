import React, { useCallback } from 'react';
import styled from 'styled-components/native';
import Colors, { fixedColors }from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import Button from '../General/Button';
import { ContentText } from '../StyledText';
import Icon from '../General/Icon';
import FilterBlock from './FilterBlock';
import iconCheck from '../../assets/images/icons/action-check.png';

const Wrapper = styled(FilterBlock)`
padding: 0;
`;

const BrandButton = styled(Button)`
height: 56px;
padding: 0 16px;
justify-content: space-between;
`;

const ButtonTitle = styled(ContentText)`
font-weight: 600;
`;

const CheckBox = styled.View`
width: 24px;
height: 24px;
border: 1.5px solid black;
border-radius: 4px;
`;

export default function FilterBrandButton({ title, active, onChange }) {
    const colorScheme = useColorScheme();
    const { divider } = Colors[colorScheme];
  
    const onPress = useCallback(() => {
      onChange(title);
    }, [ title, onChange]);

    return(
        <Wrapper style={{ borderColor: divider }}>
            <BrandButton onPress={onPress}>
                <ButtonTitle>{ title }</ButtonTitle>
                <CheckBox style={{borderColor: active ? fixedColors.orange[4]  : divider}}>
                {active && <Icon source={iconCheck} /> }
                </CheckBox>
            </BrandButton>
        </Wrapper>
    );

}