import React from 'react';
import FilterCategoryButton from './FilterCategoryButton';

const buttons = [
  { type: 'brands', title: '品牌' },
  { type: 'origins', title: '品牌地'}
]

export default function FilterCategory(props) {
  return (
    <>
      {buttons.map((b) => <FilterCategoryButton key={b.type} {...b} {...props} />)}
    </>
  );
}
