import React, { useCallback } from 'react';
import { useNavigation } from '@react-navigation/core';
import styled from 'styled-components/native';
import Colors from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import Button from '../General/Button';
import { ContentText, NoteText} from '../StyledText';
import Icon from '../General/Icon';
import FilterBlock from './FilterBlock';
import iconArrowRight from '../../assets/images/icons/arrow-right.png';


const Wrapper = styled(FilterBlock)`
  padding: 0;
`;

const TitleWrapper = styled.View`
  flex-direction: column;
`;

const CategoryButton = styled(Button)`
  padding: 16px;
  justify-content: space-between;
`;

const ButtonTitle = styled(ContentText)`
  font-weight: 600;
`;

const PngIcon = styled(Icon)`
  width: 24px;
  height: 24px;
  margin: 0;
`;

const Subtitle = styled(NoteText)`
  margin: 4px 0 0;
  padding: 0 0 0 2px;
`;

export default function FilterCategoryButton({data, type , title }) {
  const navigation = useNavigation();
  const colorScheme = useColorScheme();
  const { link, divider, text } = Colors[colorScheme];

  const onBrandPress = useCallback(() => {
    navigation.navigate('FilterMultiSelect', { type, title });
  }, [navigation, type, title]);
  
  const chooseText = data[type].join(", ");

  return(
    <Wrapper style={{ borderColor: divider }}>
      <CategoryButton onPress={onBrandPress}>
        <TitleWrapper>
          <ButtonTitle>{title}</ButtonTitle>
          { !!chooseText && <Subtitle style={{ color: link }}>{chooseText}</Subtitle> }
        </TitleWrapper>

        <PngIcon source={iconArrowRight} style={{ tintColor: text }} />
      </CategoryButton>
    </Wrapper>
  );
}
