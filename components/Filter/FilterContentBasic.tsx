import React from 'react';
import { initialConditions } from '../../hooks/useFilter';
import FilterRangeList from './FilterRangeList';
import { FilterContentBlockProps } from './FilterRange';

const blocks: Array<FilterContentBlockProps> = [
  { ...initialConditions.basic.metabolismEnergy, type: 'metabolismEnergy', title: '代謝能', unit: '大卡', meta: '/ 100 克' },
  { ...initialConditions.basic.moisture, type: 'moisture', title: '含水量', unit: '克', meta: '/ 100 克' },
];

export default function FilterContentBasic(props: any) {
  return <FilterRangeList type="basic" blocks={blocks} {...props} />;
}
