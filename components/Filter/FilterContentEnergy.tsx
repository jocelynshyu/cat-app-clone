import React from 'react';
import FilterRangeList from './FilterRangeList';
import { FilterContentBlockProps } from './FilterRange';

const blocks: Array<FilterContentBlockProps> = [
  { type: 'protein', title: '蛋白質' },
  { type: 'fat', title: '脂肪' },
  { type: 'carb', title: '碳水化合物' },
].map(b => ({ ...b, min: 0, max: 100, unit: '%' }));

export default function FilterContentEnergy(props: any) {
  return <FilterRangeList type="metabolismEnergy" blocks={blocks} {...props} />;
}
