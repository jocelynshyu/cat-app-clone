import React, { useCallback }from 'react';
import FilterOrderButton from './FilterOrderButton';
import styled from 'styled-components/native';
import Colors from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import FilterBlock from './FilterBlock';

const buttons = [
  { type: 'protein', title: '蛋白質' },
  { type: 'fat', title: '脂肪'},
  { type: 'carb', title: '碳水'}
]

const OrderButtons = styled(FilterBlock)`
  display: flex;
  flex-direction: row;
  justify-content: center;
  padding: 20px 16px;
`;

export default function FilterSort({ data, onChange }) {
  const colorScheme = useColorScheme();
  const { divider } = Colors[colorScheme];

  const onOrderChange = useCallback((type, asc) => {
    onChange('order', {type, asc});
  }, [onChange]);

  return(
    <OrderButtons style={{ borderColor: divider }}>
      {
        buttons.map(({type, title}) =>
          <FilterOrderButton
            key={type}
            type={type}
            title={title}
            data={data}
            onChange={onOrderChange}
          />
        )
      }
    </OrderButtons>
  );
}
