import React, { useCallback, useMemo} from 'react';
import styled from 'styled-components/native';
import Colors, { fixedColors }from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import Button from '../General/Button';
import { ContentText } from '../StyledText';
import Icon from '../General/Icon';
import iconArrowAsc from '../../assets/images/icons/arrow-asc.png';

const OrderButton = styled(Button)`
  flex-grow: 1;
  width: 20%;
  height: 40px;
  margin: 0 6px;
  border: 1.5px solid;
  border-radius: 4px;
`;

const ButtonTitle = styled(ContentText)`
  font-weight: 600;
  margin: 0 auto;
`;

const IconArrowAsc = styled(Icon)`
  width: 10px;
  height: 24px;
  margin: 0 12px 0 -4px;
`;

export default function FilterOrderButton({type, title, data, onChange}) {
  const asc = !data || data.type !== type ? undefined : data.asc;
  const colorScheme = useColorScheme();
  const { divider, buttonBackground, background } = Colors[colorScheme];
  const showIcon = asc !== undefined;
  const buttonStyle = showIcon ?
    { backgroundColor: background, borderColor: fixedColors.orange[4] } :
    { backgroundColor: buttonBackground, borderColor: divider };

  const iconArrowAscStyle = asc ? { transform: [{ rotateX: '180deg' }] } : undefined;

  const onOrderButtonPress = useCallback(() => {
    onChange(type, !asc);
  }, [type, asc, onChange]);

  return(
    <OrderButton onPress={onOrderButtonPress} style={buttonStyle}>
      <ButtonTitle>{title}</ButtonTitle>
      {showIcon && <IconArrowAsc source={iconArrowAsc} style={iconArrowAscStyle} />}
    </OrderButton>
  );
}
