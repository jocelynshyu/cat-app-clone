import React, { useState, useCallback, useEffect } from 'react';
import RangeSlider from 'rn-range-slider';
import styled from 'styled-components/native';

import Colors, { fixedColors } from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import { ContentText, NoteText } from '../StyledText';
import FilterBlock from './FilterBlock';

const Title = styled(ContentText)`
  font-weight: 600;
`;

const Subtitle = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  height: 20px;
  margin: 12px 0;
  padding: 0 0 0 2px;
`;

const Meta = styled(NoteText)`
  align-self: flex-end;
`;

const Thumb = styled.View`
  width: 24px;
  height: 24px;
  border-width: 4px;
  border-color: ${fixedColors.orange[4]};
  border-radius: 15px;
`;

const RailSelected = styled.View`
  height: 6px;
  background: ${fixedColors.orange[4]};
`;

const Rail = styled(RailSelected)`
  flex-grow: 1;
  margin: 0 -9px;
  border-radius: 10px;
`;

export interface DataProps {
  min?: number;
  max?: number;
}

export interface FilterContentBlockProps {
  type: string;
  title: string;
  unit: string;
  meta?: string;
  min: number;
  max: number;
}

export interface FilterRangeProps extends FilterContentBlockProps {
  data?: DataProps,
  onChange: (type: string, range: object) => void;
  setScroll: (scroll: boolean) => void;
}

export default function FilterRange({ type, title, unit, meta, min, max, data, onChange, setScroll }: FilterRangeProps) {
  const colorScheme = useColorScheme();
  const { link, meta: metaColor, divider, background } = Colors[colorScheme];

  const [low, setLow] = useState(data?.min || min);
  const [high, setHigh] = useState(data?.max || max);

  const renderThumb = useCallback(() => <Thumb style={{ backgroundColor: background }} />, []);
  const renderRail = useCallback(() => <Rail style={{ backgroundColor: divider }} />, []);
  const renderRailSelected = useCallback(() => <RailSelected />, []);

  const updateData = useCallback(() => {
    onChange(type, { min: low, max: high });
  }, [type, low, high, onChange]);
  const onTouchStart = useCallback(() => setScroll(false), [setScroll]);
  const onTouchEnd = useCallback(() => {
    setScroll(true);
    onChange(type, { min: low, max: high });

    setTimeout(updateData, 300);
  }, [setScroll, type, low, high, onChange, updateData]);
  const onValueChanged = useCallback((l, h) => {
    setLow(l);
    setHigh(h);
  }, [type]);

  useEffect(() => {
    setLow(data.min || min);
    setHigh(data.max || max);
  }, [data]);

  const rangeText = `${low} ${unit} - ${high} ${unit}`;

  return (
    <FilterBlock style={{ borderBottomColor: divider }}>
      <Title>{title}</Title>
      <Subtitle>
        <NoteText style={{ color: link }}>{rangeText}</NoteText>
        <Meta style={{ color: metaColor }}>{meta}</Meta>
      </Subtitle>
      <RangeSlider
        min={min}
        max={max}
        low={low}
        high={high}
        step={1}
        renderThumb={renderThumb}
        renderRail={renderRail}
        renderRailSelected={renderRailSelected}
        onValueChanged={onValueChanged}
        onTouchStart={onTouchStart}
        onTouchEnd={onTouchEnd}
      />
    </FilterBlock>
  );
}
