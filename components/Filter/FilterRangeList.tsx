import React, { useCallback } from 'react';

import FilterRange, { DataProps, FilterRangeProps } from './FilterRange';

export interface FilterRangeListProps {
  type: string;
  blocks: [FilterRangeProps];
  data: DataProps;
  onChange: (type: string, data: object) => void;
}

export default function FilterRangeList({ type, blocks, data, onChange, ...props }: FilterRangeListProps) {
  const onDataChange = useCallback((t, newData) => {
    onChange(type, { ...data, [t]: newData });
  }, [type, data, onChange]);

  return (
    <>
      {blocks.map((b) =>
        <FilterRange
          key={b.type}
          {...b}
          {...props}
          data={data[b.type]}
          onChange={onDataChange}
        />)
      }
    </>
  );
}
