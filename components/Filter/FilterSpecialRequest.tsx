import React, { useCallback } from 'react';
import FilterRequestButton from './FilterSpecialRequestButton';

const buttons = [
  { type: 'cereals', title: '無穀' },
  { type: 'thickener', title: '無膠／增稠劑'},
  // TODO: 後端有欄位後再開起來
  // { type: 'meatMain', title: '肉是主要成分'},
  // { type: 'proteinClear', title: '蛋白質來源標示清楚'},
  // { type: 'byProducts', title: '不含動物副產品'},
]

export default function FilterSort({ data, onChange }) {
  const onRequestChange = useCallback((type, active) => {
    onChange('request', { ...data, [type]: active });
  }, [onChange]);

  return (
    <>
      {buttons.map(({ type, title }) =>
        <FilterRequestButton
          key={type}
          type={type}
          title={title}
          active={data[type]}
          onChange={onRequestChange}
        />
      )}
    </>
  );
}
