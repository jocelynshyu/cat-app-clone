import _ from 'lodash';
import React, { useCallback, useMemo } from 'react';
import styled from 'styled-components/native';

import Colors, { fixedColors } from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import { initialConditions } from '../../hooks/useFilter';
import { NoteText } from '../StyledText';
import Button from '../General/Button';
import Icon from '../General/Icon';
import iconDelete from '../../assets/images/icons/action-x.png';

const Wrapper = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  padding: 0 2px 12px 12px;
`;

const Tag = styled(Button)`
  flex-direction: row;
  align-items: center;
  height: 28px;
  margin: 8px 8px 0 0;
  padding: 0 4px 0 8px;
  border: 1.5px solid ${fixedColors.orange[4]};
  border-radius: 4px;
`;

const Text = styled(NoteText).attrs({
  numberOfLines: 1,
})``;

const PngIcon = styled(Icon)`
  width: 16px;
  height: 16px;
  margin: 0 0 0 4px;
  tint-color: ${fixedColors.orange[4]};
`;

const texts = {
  protein: '蛋白質',
  fat: '脂肪',
  carb: '碳水化合物',
  cereals: '無穀',
  thickener: '無膠／增稠劑',
  meatMain: '肉是主要成分',
  proteinClear: '蛋白質來源標示清楚',
  byProducts: '不含動物副產品',
  brands: '品牌',
  origins: '品牌地',
};

const tags = [
  { type: 'order', text: ({ type, asc }) => `${texts[type]}：${asc ? '由低至高' : '由高至低'}` },
  {
    type: 'category',
    nodes: ['brands', 'origins'].map(type => ({
      type,
      text: (arr) => `${texts[type]}：${arr.join(',')}`,
    })),
  }, {
    type: 'request',
    nodes: ['cereals', 'thickener', 'meatMain', 'proteinClear', 'byProducts'].map((type) => ({
      type,
      text: (checked) => checked ? texts[type] : '',
    })),
  }, {
    type: 'basic',
    nodes: [
      { type: 'metabolismEnergy', text: ({ min, max }) => `代謝能 ${min}-${max}` },
      { type: 'moisture', text: ({ min, max }) => `含水量 ${min}-${max}` },
    ],
  }, {
    type: 'metabolismEnergy',
    nodes: [
      { type: 'protein', text: ({ min, max }) => `蛋白質 ${min}-${max}` },
      { type: 'fat', text: ({ min, max }) => `脂肪 ${min}-${max}` },
      { type: 'carb', text: ({ min, max }) => `碳水化合物 ${min}-${max}` },
    ],
  },
];

export default function FilterTags({ conditions, onChange }) {
  const colorScheme = useColorScheme();
  const { cardBackground } = Colors[colorScheme];

  const TagButton = styled(Tag)`background: ${cardBackground};`;

  const onPress = useCallback(({ type, parentType, text }, isMultiSelect) => {
    if (isMultiSelect) {
      onChange(text);
      return;
    }

    const defaultValue = parentType ? initialConditions[parentType][type] : initialConditions[type];

    const [t, d] = parentType ?
      [parentType, { ...conditions[parentType], [type]: defaultValue }] :
      [type, defaultValue];

    onChange(t, d);
  }, [conditions, onChange]);

  const renderTag = useCallback(({ type, text }, parentType) => {
    const defaultValue = parentType ? initialConditions[parentType][type] : initialConditions[type];
    const currentValue = parentType ? conditions[parentType][type] : conditions[type];

    const isMultiSelect = typeof text === 'string';

    if (!isMultiSelect && _.isEqual(defaultValue, currentValue)) return null;

    const tagText = isMultiSelect ? text : text(currentValue);

    return (
      <TagButton
        key={`${type}-${tagText}`}
        onPress={() => onPress({ type, parentType, text }, isMultiSelect)}
      >
        <Text>{tagText}</Text>
        <PngIcon source={iconDelete} />
      </TagButton>
    );
  }, [conditions, onPress]);

  const tagNodes = useMemo(() => {
    const defaultTags = tags.map(({ type, nodes, ...props }) => {
      if (!nodes) return renderTag({ type, ...props });
      return nodes.map(n => renderTag(n, type));
    });

    const multiSelectTags = (conditions.list || [])
      .map(t => ({ type: t, text: t }))
      .map(n => renderTag(n, 'category'));

    return [...defaultTags, ...multiSelectTags];
  }, [conditions, renderTag]);

  return (
    <Wrapper>{tagNodes}</Wrapper>
  );
};
