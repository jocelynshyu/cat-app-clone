import React from 'react';
import styled from 'styled-components/native';

import Colors, { fixedColors } from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import { ContentText } from '../../components/StyledText';
import FilterBlock from './FilterBlock';

const Box = styled(FilterBlock)`
  padding: 8px 16px;
`;

const Title = styled(ContentText)`
  color: ${fixedColors.orange[5]};
  font-weight: 600;
`;

export interface FilterTitleProps {
  title: string;
}

export default function FilterTitle({ title }: FilterTitleProps) {
  const colorScheme = useColorScheme();
  const { cardBackground, divider } = Colors[colorScheme];

  return (
    <Box style={{ backgroundColor: cardBackground, borderColor: divider }}>
      <Title>{title}</Title>
    </Box>
  );
}
