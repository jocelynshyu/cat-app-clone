import styled, { css } from 'styled-components/native';

export const iconCss = css`
  margin: auto;
  resize-mode: contain;
`;

export default styled.Image`
  ${iconCss}
`;
