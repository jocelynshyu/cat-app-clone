import React from 'react';
import styled from 'styled-components/native';

import { NoteText, ContentText } from '../StyledText';
import Colors, { fixedColors } from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';

const Wrapper = styled.View`
  flex-direction: row;
  align-items: center;
  margin: 0 6px 6px 0;
  padding: 6px 10px;
  border: 1px solid;
  border-radius: 20px;
`;



export type TagTheme = 'red' | 'green' | 'yellow';

const colors: { [type in TagTheme]: string } = {
  red: fixedColors.red[5],
  green: fixedColors.green[6],
  yellow: fixedColors.orange[5],
};

const getColors = (primary: boolean, theme: TagTheme, background: string) => {
  const color = colors[theme];

  return {
    color: primary ? '#fff' : color,
    borderColor: color,
    backgroundColor: primary ? color : background,
  }
};

export interface TagProp {
  text: string;
  theme: TagTheme;
  large?: boolean;
  primary?: boolean;
  style?: any;
}

export default function Tag({ text, large, primary, theme, style }: TagProp) {
  const colorScheme = useColorScheme();
  const { background } = Colors[colorScheme];

  const { color, borderColor, backgroundColor } = getColors(!!primary, theme, background);
  const Text = !!large ? ContentText : NoteText;

  return (
    <Wrapper style={[style, { borderColor, backgroundColor }]}>
      <Text style={{ color, marginTop: primary ? -1 : 0 }}>{text}</Text>
    </Wrapper>
  );
};
