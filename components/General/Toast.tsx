import React, { useState, useCallback, useEffect, useRef } from 'react';
import { Animated } from 'react-native';
import styled from 'styled-components/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import { fixedColors } from '../../constants/Colors';
import useToast from '../../hooks/useToast';
import useCollection from '../../hooks/useCollection';
import { NoteText, ContentText } from '../StyledText';
import Button from './Button';
import Icon from './Icon';
import iconV from '../../assets/images/icons/toast-check.png';
import iconX from '../../assets/images/icons/action-x.png';

const Wrapper = styled(Animated.View)`
  position: absolute;
  left: 8px;
  right: 8px;
  bottom: 70px;
  flex-direction: row;
  align-items: center;
  height: 48px;
  padding: 0 16px;
  border-radius: 10px;
  box-shadow: 0 2px 6px rgba(0, 0, 0, 0.16);
`;

const Title = styled(NoteText)`
  margin-right: auto;
  color: ${fixedColors.black[0]};
`;

const Undo = styled(ContentText)`
  margin-right: 12px;
  color: ${fixedColors.black[0]};
`;

const CloseButton = styled(Button)`
  width: 30px;
  height: 30px;
`;

const CloseIcon = styled(Icon)`
  width: 20px;
  height: 20px;
  tint-color: ${fixedColors.black[0]};
`;

const Check = styled(CloseIcon)`
  margin: 0 10px 0 0;
`;

function Toast() {
  const { bottom: safeInset } = useSafeAreaInsets();
  const [toast, setToast] = useState(undefined);
  const [show, setShow] = useState(false);

  const opacity = useRef(new Animated.Value(0)).current;

  const { data, remove } = useToast();

  const categories = {
    collection: useCollection(),
  };

  useEffect(() => {
    if (!data || !data.timestamp) return;

    const timer = setTimeout(() => {
      if (remove) remove();
    }, 5000);

    return () => {
      clearTimeout(timer);
    };
  }, [data]);

  useEffect(() => {
    const hasData = !!data && !!data.timestamp;
    const value = opacity.__getValue();
    const toValue = show ? 1 : 0;

    const quickHide = hasData && value === 1 && toValue === 0;

    Animated.timing(opacity, {
      toValue,
      duration: quickHide ? 0 : 300,
      useNativeDriver: true,
    }).start(() => {
      if (!show) setToast(undefined);
    });
  }, [opacity, data, show]);

  useEffect(() => {
    const hasData = !!data && !!data.timestamp;

    // 無資料 => 無資料
    if (!toast && !hasData) return;

    // 無資料 => 有資料
    if (!toast && hasData) {
      setToast(data);
      return;
    }

    // 有資料，已同步
    if (toast && hasData && toast.timestamp === data.timestamp) {
      if (!show) setShow(true);
      return;
    }

    if (show) {
      setShow(false);
    }
  }, [toast, show, data]);



  const undo = useCallback(() => {
    if (!toast || !toast.undo) return;

    const { category, action, params } = toast.undo;
    categories[category][action](...params);
    remove();
  }, [categories, toast]);

  const title = toast ? toast.title : '';
  const isSuccess = toast && toast.type === 'success';
  const backgroundColor = isSuccess ? fixedColors.green[6] : fixedColors.black[7];

  return (
    <Wrapper
      pointerEvents={show ? 'auto' : 'none'}
      style={{ bottom: 78 + safeInset, backgroundColor, opacity }}
    >
      {isSuccess && <Check source={iconV} />}
      <Title>{title}</Title>
      {toast && toast.undo && (
        <Button onPress={undo}>
          <Undo>復原</Undo>
        </Button>
      )}
      <CloseButton onPress={remove}>
        <CloseIcon source={iconX} />
      </CloseButton>
    </Wrapper>
  );
}

export default Toast;
