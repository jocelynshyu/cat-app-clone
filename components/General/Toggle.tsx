import React, { useCallback, useMemo } from 'react';
import styled from 'styled-components/native';

import Colors from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import Icon from './Icon';
import { ToggleCard } from '../Card/CardWrapper';
import { TitleText } from '../StyledText';
import iconArrowUp from '../../assets/images/icons/arrow-up.png'
import iconArrowDown from '../../assets/images/icons/arrow-down.png'

const Header = styled(ToggleCard)`
  margin: 0 0 10px;
  padding: 12px;
`;

const Content = styled.View`
  margin: -2px 0 10px;
  padding: 16px;
`;

const Arrow = styled(Icon)`
  width: 24px;
  height: 22px;
  margin: 0 0 0 auto;
`;

export interface ToggleProps {
  type: string;
  title: string;
  active: boolean;
  onPress: (type: string) => void;
  children: React.ReactNode;
}

export default function Toggle({
  type, title, active, onPress, children,
}: ToggleProps) {
  const colorScheme = useColorScheme();
  const { text, cardBackground } = Colors[colorScheme];

  const onToggle = useCallback(() => onPress(type), [type, onPress]);

  const content = useMemo(() => {
    if (!active || !children) return null;
    return <Content>{children}</Content>;
  }, [active, children]);

  return (
    <>
      <Header
        style={{ backgroundColor: cardBackground }}
        onPress={onToggle}
      >
        <TitleText style={{ color: text }}>
          {title}
        </TitleText>
        <Arrow
          style={{ tintColor: text }}
          source={active ? iconArrowUp : iconArrowDown}
        />
      </Header>
      {content}
    </>
  );
}
