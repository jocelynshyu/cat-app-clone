import React, { useCallback } from 'react';
import { useNavigation } from '@react-navigation/core';
import styled from 'styled-components/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import Colors from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import { TitleText, ContentText } from '../StyledText';
import Button from '../../components/General/Button';
import Icon from '../../components/General/Icon';
import iconArrowLeft from '../../assets/images/icons/arrow-left.png';

const Bar = styled.View`
  z-index: 2;
  box-shadow: 0 2px 2px rgba(102, 102, 102, 0.4);
  elevation: 5;
`;

const Main = styled.View`
  height: 65px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const Back = styled(Button)`
  position: absolute;
  left: 0;
  width: 50px;
  height: 50px;
  padding: 0 5px 0 0;
`;

const BackIcon = styled(Icon)`
  width: 24px;
  height: 24px;
  box-shadow: 0 2px 2px rgba(255, 255, 255, 0.4);
`;

const Action = styled(Button)`
  position: absolute;
  right: 8px;
  padding: 8px;
`;

export interface TopbarProps {
  title: string;
  action?: {
    text: string;
    onPress: () => Promise<void> | void;
  };
  additions?: React.ReactNode;
};

export default function Topbar({ title, action, pure, additions = null }: TopbarProps) {
  const colorScheme = useColorScheme();
  const { text, tint, background, shadow } = Colors[colorScheme];

  const navigation = useNavigation();
  const onBackPress = useCallback(() => navigation.goBack(), [navigation]);

  const { top: safeInset } = useSafeAreaInsets();

  return (
    <Bar style={{
      backgroundColor: background,
      paddingTop: safeInset,
      shadowColor: shadow,
    }}>
      <Main>
        {!pure && (
          <Back onPress={onBackPress}>
            <BackIcon style={{ tintColor: text, shadowColor: background }} source={iconArrowLeft} />
          </Back>
        )}

        <TitleText style={{ fontWeight: 'bold' }}>{title}</TitleText>

        {action && (
          <Action onPress={action.onPress}>
            <ContentText style={{ color: tint }}>{action.text}</ContentText>
          </Action>
        )}
      </Main>
      {additions}
    </Bar>
  );
}
