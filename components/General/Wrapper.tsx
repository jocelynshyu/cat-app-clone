import styled from 'styled-components/native';

const Wrapper = styled.View`
  flex: 1;
`;

export const CenterWrapper = styled(Wrapper)`
  align-items: center;
  justify-content: center;
`;

export default Wrapper;
