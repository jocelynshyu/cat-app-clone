import * as React from 'react';

import { Text, TextProps } from './Themed';

export function LargeTitleText(props: TextProps) {
  return <Text {...props} style={[props.style, { fontSize: 24, lineHeight: 36 }]} />;
}

export function TitleText(props: TextProps) {
  return <Text {...props} style={[props.style, { fontSize: 17, lineHeight: 26 }]} />;
}

export function ContentText(props: TextProps) {
  return <Text {...props} style={[props.style, { fontSize: 15, lineHeight: 23 }]} />;
}

export function NoteText(props: TextProps) {
  return <Text {...props} style={[props.style, { fontSize: 13, lineHeight: 20 }]} />;
}

export function MonoText(props: TextProps) {
  return <Text {...props} style={[props.style, { fontFamily: 'space-mono' }]} />;
}
