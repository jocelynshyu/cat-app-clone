const tintColorLight = '#2f95dc';
const tintColorDark = '#fff';

export const fixedColors = {
  //               0          1          2          3          4          5          6          7          8
  orange: ['#fdfcf9', '#fff7de', '#ffecb2', '#ffdd76', '#ffcf3d', '#ffbb0f', '#cb8c12', '#ac7406', '#b0640a'],
  blue:   ['#d8fdf9', '#b5fcfb', '#94f3fb', '#7be2f8', '#5ec8f4', '#489dd1', '#3576ae', '#24548c', '#183c73'],
  green:  ['#f4fddb', '#e8fbb9', '#d6f796', '#c2ef7d', '#a8e65b', '#88c549', '#6ba539', '#51842b', '#3e6e21'],
  red:    ['#fceee2', '#f9d8c7', '#f5bfab', '#f2a795', '#ee8173', '#cb5954', '#a94045', '#872c37', '#701e2e'],
  black:  ['#ffffff', '#efefef', '#c2c2c2', '#a3a3a3', '#858585', '#666666', '#474747', '#292929', '#0a0a0a'],
}

const generalColors = {
  tabIconDefault: '#ccc',
  link: '#489dd1',
};

export default {
  light: {
    ...generalColors,
    text: fixedColors.black[7],
    meta: fixedColors.black[4],
    divider: fixedColors.black[2],
    background: fixedColors.black[0],
    tint: tintColorLight,
    tabIconSelected: tintColorLight,
    cardBackground: fixedColors.orange[0],
    subCardBackground: fixedColors.orange[0],
    buttonBackground: fixedColors.black[1],
    shadow: 'rgba(102, 102, 102, 0.4)',
  },
  dark: {
    ...generalColors,
    text: fixedColors.black[0],
    meta: fixedColors.black[2],
    divider: fixedColors.black[5],
    background: fixedColors.black[8],
    tint: tintColorDark,
    tabIconSelected: tintColorDark,
    cardBackground: fixedColors.black[7],
    subCardBackground: fixedColors.black[6],
    buttonBackground: fixedColors.black[6],
    shadow: 'rgba(0, 0, 0, 0.1)',
  },
};
