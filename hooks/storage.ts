import AsyncStorage from '@react-native-async-storage/async-storage';

export default {
  get(name: string) {
    return AsyncStorage.getItem(`@meow-noo:${name}`);
  },
  set(name: string, value: any) {
    return AsyncStorage.setItem(`@meow-noo:${name}`, JSON.stringify(value));
  },
  remove(name: string) {
    return AsyncStorage.removeItem(`@meow-noo:${name}`);
  },
  clear() {
    return AsyncStorage.clear();
  },
};
