import useSWR, { mutate } from 'swr';

import storage from './storage';
import useToast from './useToast';

const fetcher = () => storage.get('collection').then(res => {
  return res ? JSON.parse(res) : [];
});

export default function useCollection() {
  const { data, error } = useSWR('collection', fetcher);
  const { add: addToast } = useToast();

  return {
    data,
    isLoading: !error && !data,
    isError: error,
    add: async (id: string, toast?: any) => {
      await storage.set('collection', [...data, id]);
      mutate('collection');

      if (toast) addToast(toast);
    },
    remove: async (id: string, toast?: any) => {
      await storage.set('collection', data.filter((i: string) => i !== id));
      mutate('collection');

      if (toast) addToast(toast);
    },
  }
};
