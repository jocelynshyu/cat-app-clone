import useSWR, { mutate } from 'swr';

import storage from './storage';

export const initialConditions = {
  // order: undefined,
  category: { brands: [], origins: [] },
  request: { cereals: false, thickener: false, meatMain: false, proteinClear: false, byProducts: false },
  basic: { metabolismEnergy: { min: 20, max: 200 }, moisture: { min: 0, max: 100 } },
  metabolismEnergy: { protein: { min: 0, max: 100 }, fat: { min: 0, max: 100 }, carb: { min: 0, max: 100 } },
};

const fetcher = () => storage.get('tempFilter').then(res => {
  return res ? JSON.parse(res) : initialConditions;
});

const updateTmp = async (f: any) => {
  await storage.set('tempFilter', f);
  mutate('tempFilter');
};
const removeTmp = async () => {
  await storage.remove('tempFilter');
  mutate('tempFilter');
};
export function useTmpFilter() {
  const { data, error } = useSWR('tempFilter', fetcher);

  return {
    data,
    isLoading: !error && !data,
    isError: error,
    update: updateTmp,
    remove: removeTmp,
  }
};

const update = async (f: any) => {
  await storage.set('filter', f);
  mutate('filter');
};
const remove = async () => {
  await storage.remove('filter');
  mutate('filter');
};
export default function useFilter() {
  const { data, error } = useSWR('filter', fetcher);

  return {
    data,
    isLoading: !error && !data,
    isError: error,
    update,
    remove,
  }
};
