import useSWR from 'swr';
import { GraphQLClient } from 'graphql-request';
import itemsJson from './dataList.json'

const GRAGHQL_URL = 'http://meow-stage.eba-wi9xvm8e.ap-northeast-1.elasticbeanstalk.com/graphql';

const graphQLClient = new GraphQLClient(GRAGHQL_URL);

const PER_PAGE = 500;
export const GET_ITEMS = `
  query Query($skip: Int) {
    itemMany(limit: ${PER_PAGE} skip: $skip) {
      _id
      brand brand_CH
      origin origin_CH
      productName productName_CH
      flavor flavor_CH
      moisture
      metabolismEnergy
      proteinMetabolizedRatio
      fatMetabolizedRatio
      carbsMetabolizedRatio
      cereals
      thickener
    }
  }
`;
export const GET_ITEM = `
  query Query($id: MongoID!) {
    itemById(_id: $id) {
      _id
      brand_CH
      origin_CH
      productName_CH
      flavor_CH
      ingredients_CH
      type
      weight
      AAFCO_WDJ
      crudeProtein
      crudeFat
      crudeFiber
      crudeCarbs
      moisture
      crudeAsh
      calcium
      phosphorus
      sodium
      magnesium
      metabolismEnergy
      proteinMetabolizedRatio
      fatMetabolizedRatio
      carbsMetabolizedRatio
      ca_PRatio
      source_ZH
      imageUrl
      cereals
      thickener
    }
  }
`;

const pageFetcher = (query, page = 1) => graphQLClient.request(query, { skip: (page - 1) * PER_PAGE });
export const usePageGraphql = (query, page) => useSWR([query, page], pageFetcher);

// const idFetcher = (query, id) => graphQLClient.request(query, { id });
const idFetcher = (query, id) => {
  const item = itemsJson.itemMany.filter(({ _id }) => _id === id)[0];
  if (!item) throw new Error();
  return { itemById: item };
};
export const useIdGraphql = (query, id) => useSWR([query, id], idFetcher);

// const fetcher = (query, variables) => graphQLClient.request(query, variables);
const fetcher = () => itemsJson;
const useGraphql = (query, variables) => useSWR([query, variables], fetcher);

export default useGraphql;
