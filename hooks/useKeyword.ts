import useSWR, { mutate } from 'swr';

import storage from './storage';

const fetcher = () => storage.get('keyword').then(res => res ? JSON.parse(res) : '');

const update = async (keyword: any) => {
  await storage.set('keyword', keyword);
  mutate('keyword');
};

const remove = async () => {
  await storage.remove('keyword');
  mutate('keyword');
};

export default function useKeyword() {
  const { data, error } = useSWR('keyword', fetcher);

  return {
    data,
    isLoading: !error && !data,
    isError: error,
    update,
    remove,
  }
};
