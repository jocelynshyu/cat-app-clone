import _ from 'lodash';
import { useMemo } from 'react'

import useGraphql, { GET_ITEMS } from './useGraphql';
import useFilter, { initialConditions } from './useFilter';
import useKeyword from './useKeyword';

export default function useProductList() {
  const { data } = useGraphql(GET_ITEMS);
  const { data: filter } = useFilter();
  const { data: keyword } = useKeyword();

  const list = useMemo(() => {
    if (filter === undefined || !data || !data.itemMany) return undefined;
    if (!keyword && _.isEqual(initialConditions, filter)) return data.itemMany;

    const keywordList = (keyword || '').toLowerCase().split(' ');

    return (data.itemMany || [])
      .filter(({
        origin_CH, brand, brand_CH, productName, productName_CH, flavor, flavor_CH,
        cereals, thickener, metabolismEnergy, moisture,
        proteinMetabolizedRatio: protein, fatMetabolizedRatio: fat, carbsMetabolizedRatio: carb,
      }) => {
        // 搜尋
        if (keyword) {
          const source = [brand, brand_CH, productName, productName_CH, flavor, flavor_CH]
            .filter(_.identity).map(_.lowerCase).join('%%%');
          return _.reduce(keywordList, (b, k) => (b && source.indexOf(k) > -1), true);
        }

        // 篩選 - 穀類、增稠劑
        if (filter.request.cereals && cereals && cereals.length) return false;
        if (filter.request.thickener && thickener && thickener.length) return false;

        // 篩選 - 產品分類
        if (_.size(filter.category.brands) && _.indexOf(filter.category.brands, brand_CH) === -1) return false;
        if (_.size(filter.category.origins) && _.indexOf(filter.category.origins, origin_CH) === -1) return false;

        // 篩選 - 代謝能與含水量
        if (!_.isEqual(initialConditions.basic, filter.basic)) {
          if (metabolismEnergy < filter.basic.metabolismEnergy.min) return false;
          if (metabolismEnergy > filter.basic.metabolismEnergy.max) return false;
          if (moisture < filter.basic.moisture.min) return false;
          if (moisture > filter.basic.moisture.max) return false;
        }

        // 篩選 - 代謝能來源
        if (!_.isEqual(initialConditions.metabolismEnergy, filter.metabolismEnergy)) {
          if (protein < filter.metabolismEnergy.protein.min) return false;
          if (protein > filter.metabolismEnergy.protein.max) return false;
          if (fat < filter.metabolismEnergy.fat.min) return false;
          if (fat > filter.metabolismEnergy.fat.max) return false;
          if (carb < filter.metabolismEnergy.carb.min) return false;
          if (carb > filter.metabolismEnergy.carb.max) return false;
        }

        return true;
      });
  }, [data, keyword, filter]);

  const orderList = useMemo(() => {
    if (!list || !filter || !filter.order) return list;

    const { type, asc } = filter.order;
    const key = `${type}MetabolizedRatio`;
    const multiplier = asc ? 1 : -1;
    return list.sort((a, b) => (a[key] - b[key]) * multiplier);
  }, [list, filter]);

  if (filter === undefined || !data || !data.itemMany) return { isLoading: true };
  return { isLoading: false, data: orderList };
}
