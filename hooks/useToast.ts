import useSWR, { mutate } from 'swr';

import storage from './storage';

const fetcher = () => storage.get('toast').then(res => {
  return res ? JSON.parse(res) : {};
});

const add = async (toastData: object) => {
  const timestamp = new Date().getTime();
  await storage.set('toast', { ...toastData, timestamp });
  mutate('toast');
};

const remove = async () => {
  await storage.remove('toast');
  mutate('toast');
};

export default function useToast() {
  const { data, error } = useSWR('toast', fetcher);

  return {
    data,
    isLoading: !error && !data,
    isError: error,
    add,
    remove,
  }
};
