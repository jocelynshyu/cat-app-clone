export interface Numerical {
  metabolismEnergy: number;
  moisture: number;
}



export type IngredientType = 'fat' | 'protein' | 'carb';

export type Ingredient = {
  [type in IngredientType]?: number;
};



export interface Product {
  _id: string;
  brand_CH: string;
  origin_CH: string;
  productName_CH: string;
  flavor_CH: string;
  ingredients_CH: string;
  type: string;
  weight: string;
  AAFCO_WDJ: string;
  crudeProtein: number
  crudeFat: number
  crudeFiber: number
  crudeCarbs: number
  moisture: number
  crudeAsh: number
  calcium: number
  phosphorus: number
  sodium: number
  magnesium: number
  metabolismEnergy: number
  proteinMetabolizedRatio: number
  fatMetabolizedRatio: number
  carbsMetabolizedRatio: number
  ca_PRatio: number
  imageUrl: string;
  cereals: [string];
  thickener: [string];
}
