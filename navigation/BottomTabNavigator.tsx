/**
 * Learn more about createBottomTabNavigator:
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */

import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import * as React from 'react';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import Colors, { fixedColors } from '../constants/Colors';
import useColorScheme from '../hooks/useColorScheme';
import ProductListScreen from '../screens/Product/ProductListScreen';
import ProductCollectionListScreen from '../screens/Product/ProductCollectionListScreen';
import ProductScreen from '../screens/Product/ProductScreen';
import FilterMainScreen from '../screens/Filter/FilterMainScreen';
import FilterMultiSelectScreen from '../screens/Filter/FilterMultiSelectScreen';
import SettingsScreen from '../screens/SettingsScreen';
import { BottomTabParamList, MainParamList, CollectionParamList, SettingsParamList } from '../types';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
  const { bottom: safeInset } = useSafeAreaInsets();

  return (
    <BottomTab.Navigator
      initialRouteName="Main"
      tabBarOptions={{
        keyboardHidesTabBar: false,
        style: {
          height: 70 + safeInset,
          paddingBottom: safeInset,
        },
        tabStyle: {
          paddingTop: safeInset ? 8 : 10,
          paddingBottom: safeInset ? 15 : 12,
          justifyContent: 'center',
        },
        activeTintColor: fixedColors.orange[5],
      }}
    >
      <BottomTab.Screen
        name="Main"
        component={MainNavigator}
        options={{
          title: '產品列表',
          tabBarIcon: ({ color }) => <TabBarIcon name="search-outline" color={color} />,
        }}
      />
      <BottomTab.Screen
        name="Collection"
        component={CollectionNavigator}
        options={{
          title: '我的收藏',
          tabBarIcon: ({ color }) => <TabBarIcon name="heart-outline" color={color} />,
        }}
      />
      {/* <BottomTab.Screen
        name="Settings"
        component={SettingsNavigator}
        options={{
          title: '帳號設定',
          tabBarIcon: ({ color }) => <TabBarIcon name="ios-settings-outline" color={color} />,
        }}
      /> */}
    </BottomTab.Navigator>
  );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function TabBarIcon(props: { name: React.ComponentProps<typeof Ionicons>['name']; color: string }) {
  return <Ionicons size={30} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const MainParamListStack = createStackNavigator<MainParamList>();

function MainNavigator() {
  const colorScheme = useColorScheme();

  return (
    <MainParamListStack.Navigator
      initialRouteName="ProductList"
      screenOptions={{
        headerShown: false,
        headerTintColor: Colors[colorScheme].text,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}
    >
      <MainParamListStack.Screen
        name="ProductList"
        component={ProductListScreen}
        options={{ headerTitle: '產品列表' }}
      />
      <MainParamListStack.Screen
        name="Product"
        component={ProductScreen}
        options={({ route }) => ({ headerTitle: route.params.title })}
      />
      <MainParamListStack.Screen
        name="FilterMain"
        component={FilterMainScreen}
        options={{ headerTitle: '篩選', gestureEnabled: false }}
      />
      <MainParamListStack.Screen
        name="FilterMultiSelect"
        component={FilterMultiSelectScreen}
        options={{ headerTitle: '' }}
      />
    </MainParamListStack.Navigator>
  );
}

const CollectionParamListStack = createStackNavigator<CollectionParamList>();

function CollectionNavigator() {
  const colorScheme = useColorScheme();

  return (
    <CollectionParamListStack.Navigator
      initialRouteName="ProductCollectionList"
      screenOptions={{
        headerShown: false,
        headerTintColor: Colors[colorScheme].text,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}
    >
      <CollectionParamListStack.Screen
        name="ProductCollectionList"
        component={ProductCollectionListScreen}
        options={{ headerTitle: '產品列表' }}
      />
    </CollectionParamListStack.Navigator>
  );
}

const SettingsStack = createStackNavigator<SettingsParamList>();

function SettingsNavigator() {
  return (
    <SettingsStack.Navigator>
      <SettingsStack.Screen
        name="Settings"
        component={SettingsScreen}
      />
    </SettingsStack.Navigator>
  );
}
