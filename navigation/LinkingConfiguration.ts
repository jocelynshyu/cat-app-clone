/**
 * Learn more about deep linking with React Navigation
 * https://reactnavigation.org/docs/deep-linking
 * https://reactnavigation.org/docs/configuring-links
 */

import * as Linking from 'expo-linking';

export default {
  prefixes: [Linking.makeUrl('/')],
  config: {
    screens: {
      Root: {
        screens: {
          Main: {
            screens: {
              ProductList: 'ProductList',
              Product: 'Product',
            },
          },
          Collection: {
            screens: {
              ProductCollectionList: 'ProductCollectionList',
            },
          },
          Settings: {
            screens: {
              Settings: 'Settings',
            },
          },
        },
      },
      NotFound: '*',
    },
  },
};
