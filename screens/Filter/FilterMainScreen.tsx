import _ from 'lodash';
import React, { useState, useCallback, useMemo, useEffect } from 'react';
import { useNavigation } from '@react-navigation/core';
import styled from 'styled-components/native';

import Colors, { fixedColors } from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import useFilter, { useTmpFilter, initialConditions } from '../../hooks/useFilter';
import { ContentText } from '../../components/StyledText';
import Wrapper from '../../components/General/Wrapper';
import Topbar from '../../components/General/Topbar';
import Button from '../../components/General/Button';
import FilterTags from '../../components/Filter/FilterTags';
import FilterTitle from '../../components/Filter/FilterTitle';
import FilterContentBasic from '../../components/Filter/FilterContentBasic';
import FilterContentEnergy from '../../components/Filter/FilterContentEnergy';

import FilterOrder from '../../components/Filter/FilterOrder';
import FilterCategory from '../../components/Filter/FilterCategory';
import FilterSpecialRequest from '../../components/Filter/FilterSpecialRequest';

const Paper = styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false,
})``;

const Footer = styled.View`
  z-index: 2;
  box-shadow: 0 -2px 2px rgba(102, 102, 102, 0.2);
  elevation: 5;
  flex-direction: row;
  height: 95px;
  padding: 24px 16px;
`;

const SubmitButton = styled(Button)`
  flex-grow: 1;
  background: ${fixedColors.orange[4]};
  border-radius: 10px;
`;

const SubmitText = styled(ContentText)`
  color: ${fixedColors.black[0]};
  font-weight: 600;
  letter-spacing: 1.5px;
`;

export default function FilterMainScreen() {
  const navigation = useNavigation();
  const colorScheme = useColorScheme();
  const { background } = Colors[colorScheme];

  const [scroll, setScroll] = useState(true);

  const { data: tmpConditions, update: updateTmp } = useTmpFilter();
  const { data: lastConditions, update } = useFilter();

  useEffect(() => {
    updateTmp(lastConditions);
  }, [lastConditions]);

  const onChange = useCallback((type: string, data) => {
    updateTmp({ ...tmpConditions, [type]: data });
  }, [tmpConditions, updateTmp]);

  const onSubmit = useCallback(() => {
    update(tmpConditions);
    navigation.navigate('ProductList');
  }, [tmpConditions, update]);

  const hasChange = useMemo(() => {
    if (!tmpConditions) return false;
    return !_.isEqual(initialConditions, tmpConditions);
  }, [tmpConditions]);

  const topbarAction = useMemo(() => {
    if (!hasChange) return;
    return { text: '全部清除', onPress: () => updateTmp(initialConditions) };
  }, [hasChange, updateTmp]);

  const topbarAdditions = useMemo(() => {
    if (!hasChange) return;
    return <FilterTags conditions={tmpConditions} onChange={onChange} />;
  }, [hasChange, tmpConditions, onChange]);

  if (!tmpConditions) return null;

  return (
    <Wrapper style={{ backgroundColor: background }}>
      <Topbar title="篩選" action={topbarAction} additions={topbarAdditions} />
      <Paper scrollEnabled={scroll}>
        <FilterTitle title="排序 (代謝能來源比例)" />
        <FilterOrder data={tmpConditions.order} onChange={onChange} />

        <FilterTitle title="產品分類" />
        <FilterCategory data={tmpConditions.category} />

        <FilterTitle title="特別要求" />
        <FilterSpecialRequest data={tmpConditions.request} onChange={onChange} />

        <FilterTitle title="代謝能與含水量" />
        <FilterContentBasic data={tmpConditions.basic} onChange={onChange} setScroll={setScroll} />

        <FilterTitle title="代謝能來源" />
        <FilterContentEnergy data={tmpConditions.metabolismEnergy} onChange={onChange} setScroll={setScroll} />
      </Paper>
      <Footer style={{ backgroundColor: background }}>
        <SubmitButton onPress={onSubmit}>
          <SubmitText>搜尋</SubmitText>
        </SubmitButton>
      </Footer>
    </Wrapper>
  );
}
