import _ from 'lodash';
import React, { useState, useCallback, useMemo, useEffect} from 'react';
import { useRoute } from '@react-navigation/native';
import { useNavigation } from '@react-navigation/core';
import Wrapper from '../../components/General/Wrapper';
import Colors from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import useGraphql, { GET_ITEMS } from '../../hooks/useGraphql';
import SearchBar from '../../components/Bar/SearchBar';
import BrandButton from '../../components/Filter/FilterBrandButton';
import { useTmpFilter, initialConditions } from '../../hooks/useFilter';
import Topbar from '../../components/General/Topbar';
import FilterTags from '../../components/Filter/FilterTags';

const fields = {
  brands: 'brand_CH',
  origins: 'origin_CH',
};

export default function FilterMultiSelectScreen() {
  const route = useRoute();
  const { type, title } = route.params;

  const { data } = useGraphql(GET_ITEMS);

  const colorScheme = useColorScheme();
  const { background } = Colors[colorScheme];
  const { data: tmpFilter, isLoading, update } = useTmpFilter();
  const navigation = useNavigation();
  const [list, setList] = useState([]);

  useEffect(() => {
    if(isLoading || !tmpFilter) return;

    const newList = tmpFilter.category[type];
    setList(newList || []);
  }, [isLoading, tmpFilter, type]);

  const onChange = useCallback((option) => {
    const listWithoutTitle = list.filter((o) => o !== option);
    setList(list.length !== listWithoutTitle.length ? listWithoutTitle : [...list, option]);
  }, [list]);

  const onPress = useCallback(() => {
    const newFilter = {
      ...tmpFilter,
      category: { ...tmpFilter.category, [type]: list },
    };

    update(newFilter);
    navigation.navigate('FilterMain');
  }, [navigation, tmpFilter, update, list]);

  const topbarAdditions = useMemo(() => {
    if (!_.size(list)) return;
    const conditions = { ...initialConditions, list };
    return <FilterTags conditions={conditions} onChange={onChange} />;
  }, [initialConditions, list, onChange]);

  const fieldInItem = _.get(fields, [type], '');
  const buttons = _.uniq(
    _.get(data, 'itemMany', [])
      .map(({ [fieldInItem]: o }) => o),
  );

  return (
    <Wrapper style={{ backgroundColor: background }}>
      <Topbar title={title} action={{ text: "下一步", onPress }} additions={topbarAdditions} />
      {/* <SearchBar /> */}
      {
        buttons.map((t) =>
          <BrandButton
            key={t}
            title={t}
            active={list.indexOf(t) >= 0}
            onChange={onChange}
          />
        )
      }
    </Wrapper>
  );
}
