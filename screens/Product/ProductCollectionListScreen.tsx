import React, { useState, useCallback, useEffect } from 'react';
import styled from 'styled-components/native';

import { Product } from '../../model/product.interface';
import Colors from '../../constants/Colors';
import useGraphql, { GET_ITEMS } from '../../hooks/useGraphql';
import useColorScheme from '../../hooks/useColorScheme';
import useCollection from '../../hooks/useCollection';
import { TitleText } from '../../components/StyledText';
import Wrapper, { CenterWrapper } from '../../components/General/Wrapper';
import Topbar from '../../components/General/Topbar';
import CardList, { Space } from '../../components/Card/CardList';
import ProductCard from '../../components/Card/ProductCard';
import IntroductionCard from '../../components/Card/IntroductionCard';

const Title = styled(TitleText)`
  margin: 5px 8px 0 0;
  line-height: 27px;
  text-align: right;
`;

export default function ProductListScreen() {
  const colorScheme = useColorScheme();

  const [products, setProducts] = useState(undefined);
  const { data: productData } = useGraphql(GET_ITEMS);
  const { data } = useCollection();

  useEffect(() => {
    if (!productData || !productData.itemMany) return;
    setProducts(productData.itemMany);
  }, [productData]);

  if (!products) {
    return (
      <CenterWrapper>
        <TitleText style={{ marginTop: 40, marginRight: 0 }}>Loading...</TitleText>
      </CenterWrapper>
    );
  }

  const collectedProducts = (products || []).filter(({ _id }) => data.indexOf(_id) > -1);

  if (!collectedProducts.length) {
    return (
      <CenterWrapper style={{ backgroundColor: Colors[colorScheme].background }}>
        <Title style={{ marginTop: 40, marginRight: 0 }}>您目前沒有收藏任何品項</Title>
      </CenterWrapper>
    );
  }

  const collections = data.map(
    (id) => products.filter(({ _id }) => (_id === id))[0]
  ).filter(c => !!c);

  return (
    <Wrapper style={{ backgroundColor: Colors[colorScheme].background }}>
      <Topbar pure title="收藏清單" />
      <CardList>
        <Title>所有結果</Title>

        <IntroductionCard />

        {collections.map((p: Product) => <ProductCard key={p._id} {...p} />)}

        <Space />
      </CardList>
    </Wrapper>
  );
}
