import _ from 'lodash';
import React, { useState, useCallback,  useEffect, useMemo, useRef } from 'react';
import { BackHandler } from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import styled from 'styled-components/native';

import { Product } from '../../model/product.interface';
import Colors from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import useToast from '../../hooks/useToast';
import useProductList from '../../hooks/useProductList';
import { TitleText } from '../../components/StyledText';
import Wrapper, { CenterWrapper } from '../../components/General/Wrapper';
import SearchBar from '../../components/Bar/SearchBar';
import CardList, { Space } from '../../components/Card/CardList';
import ProductCard from '../../components/Card/ProductCard';
import IntroductionCard from '../../components/Card/IntroductionCard';

const Hint = styled.View`
  width: 80%;
  margin: 40px auto 0;
`;

const Title = styled(TitleText)`
  margin: 5px 8px 0 0;
  line-height: 27px;
  text-align: right;
`;

const PER_PAGE = 10;

export default function ProductListScreen() {
  const isFocused = useIsFocused();
  const colorScheme = useColorScheme();

  const listRef = useRef();

  const { data: list, isLoading } = useProductList();
  const { add: addToast, remove: removeToast } = useToast();

  const [page, setPage] = useState(1);
  const maxPage: number = _.size(list) ? Math.floor(_.size(list) / PER_PAGE) : 1;

  useEffect(() => { setPage(1); }, [list]);
  useEffect(() => {
    if (!listRef || !listRef.current) return;
    if (page === 1) listRef.current.scrollTo({ x: 0, y: 0, animated: false });
  }, [page, listRef]);
  const onScroll = useCallback(({
    nativeEvent: { layoutMeasurement, contentOffset, contentSize },
  }) => {
    const paddingBottom = 300;

    if (layoutMeasurement.height + contentOffset.y >= contentSize.height - paddingBottom) {
      setPage(Math.min(page + 1, maxPage));
    }
  }, [page, maxPage]);

  const [exiting, setExiting] = useState(0);
  useEffect(() => {
    if (isFocused) return;
    setExiting(false);
    removeToast();
  }, [isFocused, removeToast]);
  useEffect(() => {
    if (!exiting) return;
    const timer = setTimeout(() => setExiting(false), 5000);
    return () => clearTimeout(timer);
  }, [exiting]);

  useEffect(() => {
    if (!isFocused) return;

    const backAction = () => {
      if (exiting) {
        BackHandler.exitApp();
        return;
      }

      addToast({ title: '若要退出 Meownoo，請再按一次返回鍵' });
      setExiting(true);
      return true;
    };

    const backHandler = BackHandler.addEventListener('hardwareBackPress', backAction);

    return () => {
      backHandler.remove()
    };
  }, [isFocused, exiting, addToast, removeToast]);

  const pList = useMemo(() => {
    const l = _.filter(list,(p, i) => (i < page * PER_PAGE));
    return _.map(
      l,
      (p: Product) => <ProductCard key={p._id} {...p} />,
    );
  }, [list, page]);

  if (isLoading) {
    return (
      <CenterWrapper>
        <TitleText style={{ marginTop: 40, marginRight: 0 }}>Loading...</TitleText>
      </CenterWrapper>
    );
  }

  return (
    <Wrapper style={{ backgroundColor: Colors[colorScheme].background }}>
      <SearchBar showFilter />
      <CardList ref={listRef} onScroll={onScroll} scrollEventThrottle={400}>
        {
          !_.size(list) ? (
            <Hint>
              <TitleText style={{ color: Colors[colorScheme].meta }}>我們找不到你要搜尋的產品，</TitleText>
              <TitleText style={{ color: Colors[colorScheme].meta }}>請更改篩選條件或使用其他關鍵字。</TitleText>
            </Hint>
          ) : (
            <>
              <Title>所有結果</Title>
              <IntroductionCard />
              {pList}
            </>
          )
        }
        <Space />
      </CardList>
    </Wrapper>
  );
}
