import React, { useRef } from 'react';
import { Animated } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useRoute, RouteProp } from '@react-navigation/native';
import styled from 'styled-components/native';

import { MainParamList } from '../../types';
import Layout from '../../constants/Layout';
import Colors from '../../constants/Colors';
import useColorScheme from '../../hooks/useColorScheme';
import { useIdGraphql, GET_ITEM } from '../../hooks/useGraphql';
import { LargeTitleText, TitleText } from '../../components/StyledText';
import Wrapper from '../../components/General/Wrapper';
import Tag from '../../components/General/Tag';
import HeartButton from '../../components/Action/HeartButton';
import ProductDataList from '../../components/Data/DataList/ProductDataList';
import MainImage from '../../components/Card/MainImage';
import NumericalBlock from '../../components/Data/NumericalBlock';
import IngredientBlock from '../../components/Data/IngredientBlock';

const Line = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
`;

const Main = styled.View`
  margin: -10px 0 0;
  padding: 0 0 25px;
  box-shadow: 0 1px 4px rgba(102, 102, 102, 0.4);
  elevation: 2;
`;

const Block = styled.View`
  margin: 0 16px;
`;

const MainBlock = styled(Block)`
  padding: 5px 45px 5px 0;
`;

const Title = styled(LargeTitleText)`
  margin: 5px 0 2px;
`;

const Heart = styled(HeartButton)`
  position: absolute;
  top: 0;
  right: -2px;
`;

export default function ProductScreen() {
  const { top: safeInset } = useSafeAreaInsets();
  const colorScheme = useColorScheme();
  const { background, link, meta, cardBackground } = Colors[colorScheme];

  const scrollY = useRef(new Animated.Value(0)).current;

  const route = useRoute<RouteProp<MainParamList, 'Product'>>();
  const { _id } = route.params;

  const { data } = useIdGraphql(GET_ITEM, _id);

  if (!data) return null;

  const {
    brand_CH: brandName, productName_CH: productName, flavor_CH: flavorName,
    type, AAFCO_WDJ, imageUrl,
    proteinMetabolizedRatio: protein, fatMetabolizedRatio: fat, carbsMetabolizedRatio: carb,
  } = data.itemById;

  return (
    <Wrapper style={{ paddingTop: safeInset, backgroundColor: background }}>
      <Animated.ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{ paddingTop: Layout.headerHeight }}
        scrollEventThrottle={16}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { y: scrollY } } }],
          { useNativeDriver: true },
        )}
      >
        <Main style={{ backgroundColor: cardBackground }}>
          <MainBlock style={{ marginTop: 20 }}>
            <Line>
              <TitleText style={{ color: link, marginRight: 16 }}>{brandName}</TitleText>
              <TitleText style={{ color: meta }}>{productName}</TitleText>
            </Line>
            <Title>{flavorName}</Title>

            <Heart id={_id} />
          </MainBlock>

          <MainBlock>
            <Line>
              {!!type && <Tag theme="yellow" text={type} />}
              {!!AAFCO_WDJ && <Tag theme="yellow" text={AAFCO_WDJ} />}
            </Line>
          </MainBlock>

          <Block>
            <NumericalBlock {...route.params} />
          </Block>
        </Main>

        <Block>
          <IngredientBlock ingredient={{ protein, fat, carb }} />

          <ProductDataList {...data.itemById} />
        </Block>
      </Animated.ScrollView>

      <MainImage
        id={_id}
        title={flavorName}
        mainImage={imageUrl}
        scrollY={scrollY}
      />
    </Wrapper>
  );
}
