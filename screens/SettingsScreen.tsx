import * as React from 'react';
import { StyleSheet } from 'react-native';

import { Text, View } from '../components/Themed';
import { CenterWrapper } from '../components/General/Wrapper';

export default function SettingsScreen() {
  return (
    <CenterWrapper>
      <Text style={styles.title}>設定頁</Text>
    </CenterWrapper>
  );
}

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});
