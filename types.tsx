import { Product } from './model/product.interface';

export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  Main: undefined;
  Collection: undefined;
  Settings: undefined;
};

export type MainParamList = {
  ProductList: undefined;
  Product: Product;
  FilterMain: undefined;
  FilterMultiSelect: undefined;
};

export type CollectionParamList = {
  ProductCollectionList: undefined;
};

export type SettingsParamList = {
  Settings: undefined;
};
